package enhancejdkproxy;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xujian
 * 2021-05-26 11:29
 **/
public class EnhanceProxyTest {
    @Test
    public void test01() {
        UserService userService = new UserServiceImpl();

        //为目标类添加方法拦截器即要增强的逻辑
        List<MethodInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new BeforeMethodInterceptor());
        interceptors.add(new BeforeBeforeMethodInterceptor());
        interceptors.add(new AfterReturnMethodInterceptor());
        interceptors.add(new AfterAfterReturnMethodInterceptor());
        interceptors.add(new AroundMethodInterceptor());

        EnhanceInvocationHandler enhanceInvocationHandler = new EnhanceInvocationHandler(userService,interceptors);

        //生成代理对象
        UserService userService1 = (UserService) EnhanceProxy.newProxyInstance(userService.getClass().getClassLoader(),
                userService.getClass().getInterfaces(),enhanceInvocationHandler);

        System.out.println("方法返回结果："+userService1.print());
    }
}
