package alg;

/**
 * @author jarryxu
 * @version 1.0
 * @title SkipListTest.java
 * @package alg
 * @date 2023/9/28 14:17
 * @description
 */
public class SkipListTest {
    public static void main(String[] args) {
        SkipList<String> skipList = new SkipList<>(3);
        skipList.insert(1,"A");
        skipList.insert(2,"B");
        skipList.insert(3,"C");
        skipList.insert(4,"D");
        skipList.insert(6,"F");
        skipList.insert(7,"G");
        skipList.insert(8,"H");
        skipList.insert(9,"I");
        skipList.insert(10,"J");
        skipList.insert(11,"L");
        System.out.println(skipList.getCurrentLevel());

        skipList.insert(10, "K");

        System.out.println(skipList.search(10));

//        System.out.println(skipList.search(9));
    }
}
