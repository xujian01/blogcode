package alg.leetcode;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 判断是否是有效的二叉查找树
 */
public class No48 {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }
    public boolean helper(TreeNode node, Integer lower, Integer upper) {
        if (node == null) return true;
        int val = node.val;
        if (lower!=null&&lower>=val) return false;
        if (upper!=null&&upper<=val) return false;
        if (!helper(node.right,val,upper)) return false;
        if (!helper(node.left,lower,val)) return false;
        return true;
    }
    public boolean isValidBST(TreeNode root) {
        return helper(root,null,null);
    }

    public boolean isValidBST22(TreeNode root) {
        return isValidBST2(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }
    public boolean isValidBST2(TreeNode node, long lower, long upper) {
        if (node == null) {
            return true;
        }
        if (node.val <= lower || node.val >= upper) {
            return false;
        }
        return isValidBST2(node.left, lower, node.val) && isValidBST2(node.right, node.val, upper);
    }
    public boolean isValidBST1(TreeNode root) {
        Deque<TreeNode> stack = new LinkedList<>();
        TreeNode p = root;
        TreeNode pre = null;
        while (p!=null||!stack.isEmpty()) {
            while (p!=null) {
                stack.push(p);
                p = p.left;
            }
            p = stack.pop();
            if (pre!=null&&pre.val>=p.val) return false;
            pre = p;
            p = p.right;
        }
        return true;
    }
}
