package alg.leetcode;

/**
 * 判断整数是否是回文数
 * @author xujian
 * @date 2021-07-03 15:29
 **/
public class No67 {
    public boolean isPalindrome(int x) {
        if (x < 0 || (x % 10 == 0 && x != 0)) {
            return false;
        }
        int reverse = 0;
        while (x > reverse) {
            int mod = x % 10;
            x /=10;
            reverse = reverse * 10 + mod;
        }
        return reverse == x || reverse / 10 == x;
    }
}
