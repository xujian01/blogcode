package alg.leetcode;

/**
 * 最大子序和
 * @author xujian
 * @date 2021-07-04 09:55
 **/
public class No68 {
    public int maxSubArray(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        }
        int sum = nums[0];
        int max = nums[0];
        for (int i = 0; i < nums.length; i++) {
            sum = Math.max(sum,sum+nums[i]);
            max = Math.max(max,sum);
        }
        return max;
    }
    public int maxSubArray1(int[] nums) {
        int pre = 0, maxAns = nums[0];
        for (int x : nums) {
            pre = Math.max(pre + x, x);
            maxAns = Math.max(maxAns, pre);
        }
        return maxAns;
    }
}
