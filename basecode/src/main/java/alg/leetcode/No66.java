package alg.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * 无重复字符的最长子串
 * @author xujian
 * @date 2021-07-03 11:37
 **/
public class No66 {
    public int lengthOfLongestSubstring(String s) {
        if (s.length() == 0) {
            return 0;
        }
        if (s.length() == 1) {
            return 1;
        }
        int start=0,max=0;
        Map<Character,Integer> map = new HashMap<>();
        for (int i=0;i<s.length();i++) {
            Character c = s.charAt(i);
            if (map.containsKey(c)) {
                start = Math.max(start,map.get(c)+1);
            }
            map.put(c,i);
            max = Math.max(max,i-start+1);
        }
        return max;
    }
}
