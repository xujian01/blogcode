package alg.leetcode;

/**
 * 大数相加/字符串相加
 * @author xujian
 * @date 2021-07-03 10:25
 **/
public class No60 {
    public String addStrings(String num1, String num2) {
        int i = num1.length()-1;
        int j = num2.length()-1;
        int carry = 0;
        StringBuilder res = new StringBuilder();
        while(i>=0 || j>=0) {
            int n1 = i>=0 ? num1.charAt(i)-'0' : 0;
            int n2 = j>=0 ? num2.charAt(j)-'0' : 0;
            int c = n1+n2+carry;
            if(c > 9) {
                res.append(c % 10);
            } else {
                res.append(c);
            }
            carry = c / 10;
            i--;
            j--;
        }
        if(carry == 1) {
            res.append(carry);
        }
        return res.reverse().toString();
    }
}
