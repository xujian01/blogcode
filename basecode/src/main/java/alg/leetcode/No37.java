package alg.leetcode;

/**
 * 字符串转整数
 */
public class No37 {
    public int myAtoi(String str) {
        int i = 0, j = 0, len = str.length();
        boolean negative = false;
        for (i = 0; i < len; i++) {
            if ('0' <= str.charAt(i) && str.charAt(i) <= '9') {
                break;
            } else if (str.charAt(i) == '-' || str.charAt(i) == '+') {
                negative = str.charAt(i) == '-';
                i++;
                break;
            } else if (str.charAt(i) != ' ') {
                return 0;
            }
        }
        for (j = i; j < len; j++) {
            if (str.charAt(j) < '0' || '9' < str.charAt(j)) {
                break;
            }
        }
        int ret = 0;
        String num = str.substring(i, j);
        for (int x = 0; x < num.length(); x++) {
            int cur = num.charAt(x) - '0';
            if (negative) {
                //这里判断溢出的情况和第7题一样
                if (ret < Integer.MIN_VALUE / 10|| ret == Integer.MIN_VALUE / 10 && cur > 8) {
                    return Integer.MIN_VALUE;
                }
                ret = ret * 10 - cur;
            } else {
                if (ret > Integer.MAX_VALUE / 10 || ret == Integer.MAX_VALUE / 10 && cur > 7) {
                    return Integer.MAX_VALUE;
                }
                ret = ret * 10 + cur;
            }
        }
        return ret;
    }

    public int myAtoi2(String s) {
        if ("".equals(s)) {
            return 0;
        }
        int res = 0;
        boolean isOverZero = true;
        int i = 0;
        //先去掉字符串前面的空格
        for (int j = 0; j < s.length(); j++) {
            if (s.charAt(j)==' ') {
                i++;
            } else {
                break;
            }
        }
        if (i >= s.length()) {
            return 0;
        }
        //判断是正数还是负数
        if (s.charAt(i) == '-') {
            isOverZero = false;
        }
        if (s.charAt(i) == '-' || s.charAt(i) == '+') {
            i++;
        }
        for (int j = i; j < s.length(); j++) {
            if (s.charAt(j)<'0' || s.charAt(j)>'9') {
                break;
            }
            int d = s.charAt(j)-'0';
            if (res > Integer.MAX_VALUE / 10 || (res == Integer.MAX_VALUE / 10 && d > 7)) {
                res = isOverZero ? Integer.MAX_VALUE : Integer.MIN_VALUE;
                break;
            }
            res = res * 10 + d;
        }
        return isOverZero ? res : -res;
    }

    public static void main(String[] args) {
        System.out.println(new No37().myAtoi2(" "));
    }
}
