package alg;

/**
 * 一个球从high高度落下，每次落地反弹至上次反弹高度的一半，
 * 写一个函数求第count次落地时球总共经过了多少距离
 * @author xujian
 * @date 2021-07-05 18:14
 **/
public class BallFall {
    public static void main(String[] args) {
        System.out.println(fun(100,3));
    }

    /**
     * 递归
     * @param high
     * @param count
     * @return
     */
    public static double fun(int high,int count) {
        if (count == 0) return 0;
        if (count == 1) return high;
        return fun(high,count-1)+(high * Math.pow(2,1-count))*2;
    }

    /**
     * 迭代
     * @param high
     * @param count
     * @return
     */
    public static double fun1(double high,double count) {
        if (count == 0) return 0;
        if (count == 1) return high;
        double sum = high;
        for (int i = 2; i <= count; i++) {
            high = high * Math.pow(2,-1);
            sum = sum + high*2;
        }
        return sum;
    }
}
