package enhancejdkproxy;

import java.lang.reflect.Proxy;

/**
 * 增强的jdk代理
 * @author xujian
 * 2021-05-26 10:06
 **/
public class EnhanceProxy {
    /**
     * 获取代理类
     * @param loader
     * @param interfaces
     * @param h
     * @return
     */
    public static Object newProxyInstance(ClassLoader loader,
                                          Class<?>[] interfaces,
                                  EnhanceInvocationHandler h) {
        return Proxy.newProxyInstance(loader,interfaces,h);
    }
}
