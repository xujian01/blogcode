package enhancejdkproxy;

/**
 * @author xujian
 * 2021-05-26 10:11
 **/
public abstract class AbstractAfterReturnMethodInterceptor implements MethodInterceptor {
    /**
     * 执行增强
     * @param chain
     * @param joinPointInfo
     * @return
     * @throws Exception
     */
    @Override
    public final Object proceed(InterceptorChain chain, JoinPointInfo joinPointInfo) throws Exception {
        Object o = chain.proceed(joinPointInfo);
        proceed(joinPointInfo);
        return o;
    }

    /**
     * 获取连接点类型
     *
     * @return
     */
    @Override
    public final JoinPointEnum getJoinPoint() {
        return JoinPointEnum.AFTER_RETURN;
    }
}
