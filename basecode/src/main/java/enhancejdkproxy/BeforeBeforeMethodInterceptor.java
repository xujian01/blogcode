package enhancejdkproxy;

/**
 * @author xujian
 * 2021-05-26 11:35
 **/
public class BeforeBeforeMethodInterceptor extends AbstractBeforeMethodInterceptor {
    /**
     * 用户自定义增强逻辑
     *
     * @param joinPointInfo
     */
    @Override
    public void proceed(JoinPointInfo joinPointInfo) {
        System.out.println("----beforebefore");
    }
}
