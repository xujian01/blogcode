package enhancejdkproxy;

/**
 * @author xujian
 * 2021-05-26 10:11
 **/
public abstract class AbstractBeforeMethodInterceptor implements MethodInterceptor {
    /**
     * 执行增强
     * @param chain
     * @param joinPointInfo
     * @return
     * @throws Exception
     */
    @Override
    public final Object proceed(InterceptorChain chain, JoinPointInfo joinPointInfo) throws Exception {
        proceed(joinPointInfo);
        return chain.proceed(joinPointInfo);
    }

    /**
     * 获取连接点类型
     *
     * @return
     */
    @Override
    public final JoinPointEnum getJoinPoint() {
        return JoinPointEnum.BEFORE;
    }
}
