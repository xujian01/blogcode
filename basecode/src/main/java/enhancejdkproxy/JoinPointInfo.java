package enhancejdkproxy;

import java.lang.reflect.Method;

/**
 * @author xujian
 * 2021-05-27 14:24
 **/
public class JoinPointInfo {
    private Method method;
    private Object[] args;
    private Object target;
    private Object proxy;


    public static final class Builder {
        private Method method;
        private Object[] args;
        private Object target;
        private Object proxy;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder method(Method method) {
            this.method = method;
            return this;
        }

        public Builder args(Object[] args) {
            this.args = args;
            return this;
        }

        public Builder target(Object target) {
            this.target = target;
            return this;
        }

        public Builder proxy(Object proxy) {
            this.proxy = proxy;
            return this;
        }

        public JoinPointInfo build() {
            JoinPointInfo joinPointInfo = new JoinPointInfo();
            joinPointInfo.args = this.args;
            joinPointInfo.target = this.target;
            joinPointInfo.method = this.method;
            joinPointInfo.proxy = this.proxy;
            return joinPointInfo;
        }
    }

    public Method getMethod() {
        return method;
    }

    public Object[] getArgs() {
        return args;
    }

    public Object getTarget() {
        return target;
    }

    public Object getProxy() {
        return proxy;
    }
}
