package com.jarry.circledepency;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author xujian
 * 2021-06-04 16:30
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoTest {
    @Autowired
    private ServiceA serviceA;
    @Autowired
    private ServiceB serviceB;

    @Test
    public void test01() {
        ServiceB getB = serviceA.getServiceB();
        serviceA.exec("hello");
        System.out.println(getB);
        System.out.println(serviceB);
    }
}
