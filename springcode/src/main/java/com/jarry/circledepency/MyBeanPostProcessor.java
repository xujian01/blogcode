package com.jarry.circledepency;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.reflect.Proxy;

/**
 * @author xujian
 * 2021-06-04 16:22
 **/
//@Component
public class MyBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        return o;
    }

    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
        if ("serviceA".equals(s)) {
            return Proxy.newProxyInstance(o.getClass().getClassLoader(),
                    o.getClass().getInterfaces(),
                    (proxy, method, args) -> {
                        System.out.println("----开始");
                        Object o1 = method.invoke(o,args);
                        System.out.println("----结束");
                        return o1;
                    });
        } else {
            return o;
        }
    }
}
