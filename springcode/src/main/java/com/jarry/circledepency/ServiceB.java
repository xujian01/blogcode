package com.jarry.circledepency;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author xujian
 * 2021-06-04 16:21
 **/
@Service
public class ServiceB {
    @Autowired
    private ServiceA serviceA;
}
