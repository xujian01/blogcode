package com.jarry.circledepency;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author xujian
 * 2021-06-04 17:16
 **/
@Component
@Aspect
public class MyAspect {
    @Pointcut("execution(public void com.jarry.circledepency.ServiceA.exec(*))")
    public void pointCut(){}

    @Before("pointCut()")
    public void before() {
        System.out.println("----before");
    }

    @Around("pointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("----start");
        Object o = joinPoint.proceed();
        System.out.println("----end");
        return o;
    }

    @AfterReturning("pointCut()")
    public void afterReturning() {
        System.out.println("----after-return");
    }
}
